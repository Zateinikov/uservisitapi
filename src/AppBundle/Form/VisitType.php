<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Form\DataTransformer\UserToNumberTransformer;

use AppBundle\Form\UserType;


class VisitType extends AbstractType
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    public function buildForm( FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('registered', DateType::class, ["widget"=>"single_text", "format" => "yyyy-MM-dd H:mm:ss"])
            ->add('user', TextType::class, ['invalid_message' => 'It is not a correct user_id', 'label' => 'user_id'])
            ;

        $builder->get("user")->addModelTransformer( new UserToNumberTransformer($this->manager));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Visit',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return '';
    }
}