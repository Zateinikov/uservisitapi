<?php

namespace AppBundle\Form\DataTransformer;

use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;


class UserToNumberTransformer implements DataTransformerInterface
{
    private $manager;

    /**
     * Pass here as argument for form's service (app.form.visit)
     * @param ObjectManager $manager
     */
    public function __construct( ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Transform an object(user) to a integer (id)
     * @param object $user User
     * @return integer
     */
    public function transform($user) {
        if($user === null) {
            return;
        }

        return $user->getId();
    }

    /**
     * Transform user_id into User object
     * @param integer $userId
     * @return object User
     */
    public function reverseTransform($userId)
    {
        if(!$userId) {
            throw new TransformationFailedException("User id can't be empty");
        }

        $user = $this->manager->getRepository('AppBundle:User')->find($userId);
        if( $user === null) {
            throw new TransformationFailedException(sprintf("An User with id %s doesn not exist", $userId));
        }
        return $user;
    }
}
