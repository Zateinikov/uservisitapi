<?php
namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Entity\Visit;
use AppBundle\Form\VisitType;


/**
 * REST controller for Visit
 * @package AppBundle\Controller\Api
 * @author Sergo Yakushenko <sergo.yakushenko@gmail.com>
 */

class VisitController extends FOSRestController
{
    /**
     * Create a new Visit for User from submitted data
     *
     * @ApiDoc(
     *  views={"default", "visit"},
     *  section="Visit API",
     *  input = "AppBundle\Form\VisitType",
     *  statusCode={
     *     201 = "Successfully created",
     *     204 = "Returned when successful",
     *     400 = "Error of form"
     *   }
     * )
     *
     * @Annotations\View(
     *   template = "AppBundle:Api/Visit:newVisit.html.twig",
     *   statusCode = Response::HTTP_BAD_REQUEST
     * )
     *

     *
     * @param Request $request
     * @return object FormTypeInterface|View
     */

    public function postVisitAction(Request $request)
    {
        $visit = new Visit();
        $form = $this->createForm(VisitType::class, $visit);

        //$form->handleRequest($request);
        /**
         * Allow to submit form without some fields (i.e. registered field)
         */
        $form->submit($request->request->get($form->getName()), false);

        if($form->isValid()) {
            $visit = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($visit);
            $em->flush();

            return $this->routeRedirectView('get_visits');
        }

        /**
         * Code snippet for debug form's error
         */
        /*$errors = array();
        foreach ($form as $fieldName => $formField) {
            foreach ($formField->getErrors(true) as $error) {
                $errors[$fieldName] = $error->getMessage();
            }
        }*/

        return $form;
    }

    /**
     * Return form for create Visit
     *
     * @ApiDoc(
     *  views={"default", "visit"},
     *  section="Visit API",
     *  statusCode={
     *     200 = "Successful",
     *     405 = "Method not allowed"
     *   }
     * )
     * @Annotations\View()
     * @return \Symfony\Component\Form\Form
     */
    public function  newVisitAction()
    {
        $visit = new Visit();
        $form = $this->createForm(VisitType::class, $visit);
        return $form;
    }

    /**
     * List of Visits
     *
     * @ApiDoc(
     *  views={"default", "visit"},
     *  section="Visit API",
     *  statusCodes={
     *      200 = "Successful",
     *      404 = "Returned when the visits are not found",
     *  },
     *  description = "Return a collection of Visits",
     * )
     * @Annotations\View(
     *     templateVar="visits"
     * )
     * @return array
     */
    public function getVisitsAction(){
        return $this->getDoctrine()->getRepository('AppBundle:Visit')->findAll();
    }



}