<?php
namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class DauController extends FOSRestController
{
    /**
     * Return result count of unique Users in defined period of time
     *
     * @ApiDoc(
     *  views={"default", "DAU"},
     *  section="DAU API",
     *  statusCodes={
     *      200 = "Successful",
     *      404 = "Returned when the Visit is not found",
     *  },
     *  description = "Daily Active Users(DAU). Return a collection of Visits and Users",
     * )
     *
     * @Annotations\View();
     *
     * @Annotations\QueryParam(name="from_date")
     * @Annotations\QueryParam(name="to_date")
     */
    public function getDausAction(ParamFetcherInterface $paramFetcher)
    {
        $from_date = $paramFetcher->get("from_date");
        $to_date = $paramFetcher->get("to_date");

        $visits = $this->getDoctrine()->getRepository("AppBundle:Visit")->findByDate($from_date, $to_date);



        return [
            "DAU_count" => $visits,
            "from_date" => $from_date,
            "to_date" => $to_date,
        ];
    }

    /**
     * List of Visits and Users
     *
     * @ApiDoc(
     *  views={"default", "DAU"},
     *  section="DAU API",
     *  statusCodes={
     *      200 = "Successful",
     *      404 = "Returned when the Visit is not found",
     *  },
     *  description = "Return a collection of Visits and Users",
     * )
     * @Annotations\View();
     *
     * @Annotations\QueryParam(name="from_date")
     * @Annotations\QueryParam(name="to_date")
     */
    public function getDausFullAction(ParamFetcherInterface $paramFetcher)
    {
        $from_date = $paramFetcher->get("from_date");
        $to_date = $paramFetcher->get("to_date");

        $visits = $this->getDoctrine()->getRepository("AppBundle:Visit")->findByDateFull($from_date, $to_date);

        return [
            "visits" => $visits,
            "from_date" => $from_date,
            "to_date" => $to_date,
        ];

    }
}

