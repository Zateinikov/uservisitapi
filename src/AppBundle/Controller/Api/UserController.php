<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * REST controller for Users
 * @package AppBundle\Controller\Api
 * @author Sergo Yakushenko <sergo.yakushenko@gmail.com>
 */

class UserController extends FOSRestController
{

    /**
     * List of Users
     *
     * @ApiDoc(
     *  views={"default", "user"},
     *  section="User API",
     *  statusCodes={
     *      200 = "Successful",
     *      404 = "Returned when the users is not found",
     *  },
     *  description = "Return a collection of Users",
     * )
     * @Annotations\View()
     *
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many Users return.")
     */
    public function getUsersAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get("limit");

        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findBy([],[],$limit);
        if(!$users) {
            throw $this->createNotFoundException('No user');
        }

        return ["users" => $users, "limit" => $limit];
    }

    /**
     * Return form for create User
     *
     * @ApiDoc(
     *  views={"default", "user"},
     *  section="User API",
     *  statusCode={
     *     200 = "Successful",
     *     405 = "Method not allowed"
     *   }
     * )
     *
     * @Annotations\View()
     *
     * @return FormTypeInterface
     */
    public function newUserAction()
    {
        return $this->createForm(UserType::class, new User());
    }
    
    /**
     * Get a single User.
     *
     * @ApiDoc(
     *  views={"default", "user"},
     *  section="User API",
     *  output = "AppBundle\Model\Note",
     *  statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the User is not found"
     *   }
     * )
     * Annotations\View(templateVar="user")
     *
     * @param int $id
     *
     * @return array
     */
    public function getUserAction($id)
    {
        $user = $this->getUserById($id);

        /* if use  Annotations\View(templateVar="user") */
        //        return $user;

        $view = $this->view($user, 200)
            ->setTemplate("AppBundle:Api/User:getUser.html.twig")
            ->setTemplateVar('user');
        return $view;

        /* use FOS\RestBundle\View\View */
        /*
            $view = View::create();
            $view
                ->setData($user)
                ->setTemplate("AppBundle:Api/User:getUser.html.twig")
                ->setTemplateVar('user')
            ;
        return $view;
        or
        return $this->handleView($view);
        */
    }


    /**
     * Create a new User from submitted data
     *
     * @ApiDoc(
     *  views={"default", "user"},
     *  section="User API",
     *  input = "AppBundle\Form\UserType",
     *  statusCode={
     *     201 = "User added",
     *     204 = "Returned when successful",
     *     400 = "Error of form"
     *   }
     * )
     *
     * @Annotations\View(
     *   template = "AppBundle:Api/User:newUser.html.twig",
     *   statusCode = Response::HTTP_BAD_REQUEST
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface[]|View
     */
    public function postUserAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->routeRedirectView('get_user', ['id' => $user->getId()]);
        }

        return ['form' => $form ];
    }

    /**
     * Presents the form to use to update an existing User.
     *
     * @ApiDoc(
     *  views={"default", "user"},
     *  section="User API",
     *  statusCodes={
     *     200 = "Returned when successful",
     *     404 = "Returned when the User is not found"
     *   }
     * )
     *
     * @Annotations\View()
     *
     * @param int $id
     *
     * @return FormTypeInterface
     */
    public function editUserAction($id)
    {
        $user = $this->getUserById($id);

        return $this->createForm(UserType::class, $user);
    }

    /**
     * Update existing User from the submitted data or create a new User at a specific location.
     *
     * @ApiDoc(
     *  views={"default", "user"},
     *  section="User API",
     *  input = "AppBundle\Form\UserType",
     *  statusCodes = {
     *     201 = "Returned when a new resource is created",
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     * @Annotations\View(
     *      template = "AppBundle:Api/User:getUser.html.twig",
     *      templateVar = "user"
     * )
     * @param Request $request
     * @param int $id
     * @return View|FormTypeInterface
     */
    public function putUserAction(Request $request, $id)
    {
        $user = $this->getUserById($id);

        if( $user === false) {
            $user = new User();
            $statusCode = Response::HTTP_CREATED;
        } else {
            $statusCode = Response::HTTP_NO_CONTENT;
        }

        $form = $this->createForm( UserType::class, $user);

        $form->submit($request->request->get($form->getName()));
        //$form->handleRequest($request); //- Doesn't work with PUT
        /** In some cases, you want better control over when exactly your form is submitted and what data is passed to
         *  it. Instead of using the handleRequest() method, pass the submitted data directly to submit():
         *  https://symfony.com/doc/current/form/direct_submit.html#calling-form-submit-manually
         */

        if ($form->isValid()) {
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->routeRedirectView('get_user', ['id' => $user->getId()], $statusCode);
        }

        return $form;
    }

    /**
     * Removes a User.
     *
     * @ApiDoc(
     *  views={"default", "user"},
     *  section="User API",
     *  statusCodes={
     *     204="Returned when successful"
     *   }
     * )
     * @param int $id
     * @return View
     */
    public function removeUserAction($id)
    {
        return $this->deleteUserAction($id);
    }

    /**
     * Removes a User.
     *
     * @ApiDoc(
     *  views={"default", "user"},
     *  section="User API",
     *   statusCodes={
     *     204="Returned when successful"
     *   }
     * )
     * @param int $id
     * @return View
     */
    public function deleteUserAction($id)
    {
        $user = $this->getUserById($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->routeRedirectView('get_users');
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @return object User
     */
    public function getUserById($id){
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        if($user === false) {
            throw $this->createNotFoundException("Cant't find User with id = {id}");
        }
        return $user;
    }

}
